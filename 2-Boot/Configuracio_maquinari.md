### L'arrencada

Hi ha quasi tantes BIOS com ordinadors, bé tantes no, però gairebé una per cada
fabricant, la qual cosa implica que cada bios pot tenir característiques
(*funcionalitats*) diferents.

A aquesta varietat, pròpia del sistema BIOS, s'afegeix el relativament nou
sistema UEFI, que conviurà durant un bon temps amb el firmware BIOS.

##### Exercici 1.

Nosaltres treballarem amb el sistema BIOS però abans una pregunta respecte als dos sistemes BIOS/UEFI.
Quin dels dos sistemes té tota la seva informació gravada a una memòria a la placa mare
i quina té una part a una partició del disc dur?

##### Exercici 2.

Tenim por que ens introdueixen algun virus amb una memòria flash o cd o dvd.
Volem per tant que l'ordinador vagi a buscar el Sistema Operatiu al disc dur,
tot i que hi hagi una memòria flash o un cd, dvd amb sistema operatiu...

##### Exercici 3.

Ja hem configurat l'ordinador segons l'exercici anterior. Ara resulta
que volem provar una live de ArchLinux que tenim gravada a un dispositiu extern.
Com ho podríem fer sense entrar al setup de la BIOS? Podria ser el cas de que aquesta opció no existís?
O a l'inrevés, si aquesta opció existeix, no és un forat de seguretat?
I si fos així com s'hauria de resoldre aquest problema?

##### Exercici 4.

Ara volem fer una instal·lació via xarxa utilitzant el protocol PXE
a un ordinador que en principi no permet aquesta opció.
Que hauríem de fer?

Se us acut alguna altra situació on pogués ser útil aquesta opció?

##### Exercici 5.

Tenim un ordinador una mica antic que pot tenir problemes
a l'hora de reconèixer un disc dur amb interfície SATA. Que puc fer per resoldre aquest problema?

##### Exercici 6.

Per segons quines tasques realitzin alguns servidors ens podem trobar amb màquines molt velles. Suposem que tenim un servidor al qual no es connecta ningú directament. Només de manera remota.
Quin problema ens podem trobar i quina seria la solució? (si el pc no és molt vell no té sentit buscar a la BIOS)

##### Exercici 7.

Tinc una placa mare *nova* amb HD Àudio però amb una caixa-torre una mica antiga amb els connectors AC97.
Hi ha setup's que tenen present aquesta situació. Que hauria de fer?

##### Exercici 8.

Els ordinadors més moderns tenen CPU's amb extensions per virtualització.
Una manera de saber si el nostre ordinador admet virtualització o sigui si la CPU té aquesta extensió,
és executar la següent ordre:

```
egrep '^flags.*(vmx|svm)' /proc/cpuinfo
```

Si no surt res, no podem virtualitzar. Però si surten coses es que sí podem virtualitzar.
Tot i així és podria donar el cas que la virtualització no funcionés encara. Com es podria solucionar això?

##### Exercici 9.

Si el firmware fa servir l'estàndard UEFI en comptes de l'estàndard BIOS, pot fer servir l'opció _Secure Boot_.
Quina és la funcionalitat que ens proporciona Secure Boot? Escull una opció:

* Si aquesta opció està activada es fa un xequeig antivirus d'inici.
* Si aquesta opció està activada només es poden instal·lar productes Microsoft.
* Si aquesta opció està activada només es poden instal·lar binaris amb signatura.
* Cap de les anteriors és correcta (troba quina és)

