Amb l'ajuda d'internet i de l'article que teniu al final d'aquest document responeu a les següents preguntes:

##### Exercici 1.

Digues a quina generació d'ordinadors pertanyen aquests elements:

	* Vàlvules al buit 
	* Microprocessador 
	* Circuits integrats_
	* Transistors

##### Exercici 2.

Qui està considerat per a molts com l'inventor de la famosa _màquina
analítica_: el primer ordinador automàtic i programable? Va aconseguir
implementar-la?

##### Exercici 3.

Que era/és el disquet (_floppy disk_)? Quina empresa el va inventar?

##### Exercici 4.

A més d'IBM a quina altra empresa li devem en gran mesura l'existència de l'ordinador personal? A principis dels 90 va estar a punt de fer fallida. Avui en dia és HP.

##### Exercici 5.

Que deia la llei de Grosch?

##### Exercici 6.

Quin _mètode_ de connexió de xarxes es va crear al centre d'investigació de Xerox al 1973? (i va desmuntar la llei de Grosch)

##### Exercici 7.

>	Un projecte finançat per l'Agència de Projectes d'Investigació Avançada
>	en Defensa (ARPA) tenia com a objecte que les comunicacions militars es
>	mantinguessin segures en cas de guerra, quan els trams d'una xarxa
>	podien ser destruïts.
>
>	Les primeres xarxes militars que provenien del Projecte Whirlwind
>	tenien unitats de comandament central, i per això era possible atacar
>	al centre de control de la xarxa.
>
>	Aquestes unitats es trobaven en edificis sense finestres, reforçats amb
>	estructures de formigó, però si sofrien danys la xarxa deixava de
>	funcionar.
>
>	ARPA va finançar la labor d'un grup d'investigadors que van
>	desenvolupar una alternativa en la qual es va dividir la informació en
>	paquets, cadascun els quals rebia l'adreça d'un ordinador receptor i
>	circulaven a través de la xarxa d'ordinadors. Si un o més ordinadors a
>	la xarxa no funcionaven, el sistema trobaria una altra ruta. El
>	receptor reunia els paquets i els convertia en una còpia fidel del
>	document original que havia transmès.

Que acabava de nèixer?

##### Exercici 8.

Quin conjunt de protocols, que es van crear al CERN, permetien un accès
flexible i generalitzat a la informació emmagatzemada a la xarxa en diferents
formats? Qui va ser el seu principal creador?

##### Exercici 9.

Fes un diagrama de l'arquitectura de Von Neumann:

##### Exercici 10.

Quin és l'origen de la paraula _informàtica_?

Links:

* [Història de la informàtica](https://www.bbvaopenmind.com/articulo/historia-de-la-informatica)
* [Eina per fer diagrames amb format ascii](http://asciiflow.com)
