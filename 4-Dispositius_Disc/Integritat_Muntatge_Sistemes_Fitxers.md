Responeu als exercicis fent servir els enllaços següents:

* [Mantenir la integritat dels sistemes de
  fitxers](https://www.ibm.com/developerworks/library/l-lpic1-104-2/index.html)
* [Muntatge i desmuntatge de sistemes de
  fitxers](https://www.ibm.com/developerworks/library/l-lpic1-104-3/index.html)

---

##### Exercici 1.

Enumereu quina és la informació referent a un fitxer que conté l'estructura anomenada *inode*.

##### Exercici 2.

L'inode conté el nom del fitxer? Quan executem alguna de les
ordres al bash que ens proporciona algun tipus d'informació d'un fitxer, com
s'ho fa el kernel de linux per arribar a aquesta informació?

##### Exercici 3.

Com es diu la propietat que tenen alguns sistemes de fitxers per poder
implementar transaccions i poder restablir les dades del sistema de fitxers en
cas de corrupció d'aquestes dades (tal i com es fa a una base de dades).

##### Exercici 4.

Mostreu en ordre els directoris que ocupen més espai del vostre directori i
subdirectoris.

##### Exercici 5.

Mostreu els fitxers que ocupen _MB_ al vostre _HOME_

##### Exercici 6.

Mostreu ara un resum _humà_, o sigui el total ocupat al nostre directori.

##### Exercici 7.

Quina ordre ens mostrarà l'espai lliure i l'ocupat de totes les particions que
tinguem en format _llegible per humans_? I si volguéssim que al mesurar les
particions s'utilitzés que 1k en comptes de ser 2¹⁰ (o sigui 1024) fos 1000, ja
que hi ha alguns fabricants que ho fan d'aquesta manera, sistema internacional,
i així ens poden _vendre_ menys espai del que ens pensàvem?

##### Exercici 8.

Si hi ha una caiguda de tensió i s'apaga/reinicia el S.O, hi ha alguna eina que
ens ajudi a solucionar el possible problema d'inconsistència de dades? (Ens
referim a que hi hagi dades que no s'hagi acabat d'escriure)

##### Exercici 9.

Suposem que volem que a l'arrancar el sistema es faci un xequeig automàtic
d'una partició on tenim un sistema de fitxers _ext4_ (per exemple a /dev/sda5).
Com ho faràs? Recorda _man ordre/fitxer_

##### Exercici 10.

De vegades intentem desmuntar un dispositiu i el sistema no ho permet: el
dispositiu està ocupat. Amb quina ordre podria saber quins són els fitxers
oberts o quin procés té fitxers oberts?

##### Exercici 11.

Suposem que tenim una imatge iso d'un cd o dvd i volguéssim veure quin és el
seu contingut (fitxers, directoris) sense necessitat de _cremar_ aquesta iso en
un cd o dvd.  Això es pot fer amb l'ordre `mount` i l'opció `loop`. Proveu-lo
amb una iso disponible a public o us la baixeu per internet però de mida
petita.  Un cop fet això, investigueu que conté la imatge __partition_5M.iso__
(demaneu-li al professor)

---

##### Links 

URL's interessants sobre hard & softs links:

* http://linuxgazette.net/105/pitcher.html
* http://publib.boulder.ibm.com/iseries/v5r2/ic2924/index.htm?info/ifs/rzaaxmstlinkcmp.htm

Esquema interessant a l'apartat "Enlaces (Links)" de la web:

* http://www.ldc.usb.ve/~adiserio/ci3825/ClaseFS1.html
