### Arrencada equip: MBR

##### Exercici 1.

Volem ordenar el procés d'arrencada general d'un pc, des de que s'engega fins
que obtenim la pantalla de benvinguda del corresponent sistema operatiu.  Tot i
que nosaltres simplificarem una mica el procés, tenim les fases desordenades.

Hem d'ordenar-les:

* El MBR carrega el boot loader, per exemple el GRUB

* La BIOS carrega el MBR (master boot record), que normalment es troba al
  sector 0 (de 512 bytes) del disc dur (o diskette, o pen USB, CD ...)
d'arrencada. 

* El boot loader (a Linux típicament GRUB) carrega el sistema operatiu (a Linux
  típicament el kernel).

* La BIOS (basic input output service) realitza un POST (power on self test)

##### Exercici 2.

Volem fer un cop d'ull a l'aspecte del MBR (recordeu que són els primers 512
bytes del dispositiu).

Un MBR té la següent estructura:

![Estructura d'un MBR](mbr.png)


* Utilitzant l'ordre `dd` copiarem a un fitxer el MBR, és a dir els primers 512
  bytes del disc dur que conté informació necessària per arrencar el nostre
sistema operatiu. Li posarem de nom "mbr.bin".  (Opcions interessants per a
`dd` *bs* i *count*)

* Un cop hem creat aquest fitxer que conté el MBR, fem un cop d'ull amb l'ordre
  `file`.

* On es troba la informació que representa la teva taula de particions?

* Quants bytes s'utilitzen del MBR per a representar aquesta taula?

* Això ens dona alguna limitació de número de particions?

* Quantes particions veus en aquest fitxer?

* També podríem utilitzar ordres com `hexdump` o `od` per veure el contingut del fitxer, o l'explorador de fitxers _midnight commander_ `mc` per mirar el contingut d'aquest fitxer (opcions *View/Hexadec/GoTo*)

* Si escollim l'ordre `od` al `man` trobarem una opció que ens permet _saltar_ els bytes que volguem. Tanmateix podem mostrar la informació en diferents formats amb `-t` per exemple decimal utilitzant 4 bytes `-t d4` o decimal utilitzant només 1 byte `-t d1`.

	Escull alguna partició, per exemple la primera, i amb l'ordre `od` troba el codi de la partició al fitxer *mbr.bin*, així com el número del sector on comença la partició i la quantitat de sectors de la partició. Recorda que amb `fdisk -l /dev/sda` obtenim tota la informació referent a les particions.

Si no trobes un bon enllaç buscant a internet, pots mirar [aquí](https://www.bydavy.com/2012/01/lets-decrypt-a-master-boot-record/)

Si no es veu la imatge podeu accedir a [l'enllaç original de la viquipèdia](https://ca.wikipedia.org/wiki/Master_boot_record)
