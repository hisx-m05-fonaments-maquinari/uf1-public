### Arrencada equip: GRUB

#### GRUB shell

1. Desem una còpia de seguretat del menú del *grub* (just in case).  Això és
   recomanable que ho fem sempre, al següent exercici ja veurem el perquè.	

2. Comprovem que aquesta còpia realment funciona. Si el fitxer *grub.cfg* es
   corromp una de les possibilitats és que ens enviï a una shell del grub. Anem
a emular-ho.

	* Reinicia el sistema
	* Prem la tecla 'c' tal i com indica el grub: *... or 'c' for a command prompt*
	* Ajudeu-vos del tabulador per fer l'AUTOCOMPLETE en les següents instruccions.
	* Llista les particions (i disc dur) disponibles: ```ls```
	```
	grub> ls
	(proc) (hd0) (hd0,msdos1) (hd0,msdos5) ...
	```
	* Llistem la partició del matí:
	```
	grub> ls (hd0,msdos5)
	```
	Així com:
	```
	grub> ls (hd0,msdos5)/
	```
	* Carreguem el fitxer de configuració (tipus grub.cfg) amb l'ordre ```configfile```
	gairebé ja la tenim aquí, només manca substituir els interrogants:
	```
	grub> configfile (hd0,msdos?)/boot/grub2/?

	Si arrenca el grub és que el fitxer de backup és correcte.
	
3. Anem a simular una situació pitjor. No trobem cap fitxer de configuració
   grub que funcioni bé.  Volem arrencar Fedora de manera manual. Fem abans un
cop d'ull al fitxer *grub.cfg* fixant-nos concretament en 3 instruccions molt
importants:

	```
	... # pregunta extra: recordeu que feia l'ordre _insmod_?
	set root ...
	linux /boot ...
	initrd /boot ....
	...
	```

	Farem el següent, també des de la shell del GRUB, ajudant-nos amb el tabulador per autocompletar:
	
	* Activem la partició a on es troba el sistema de fitxers /:  
	```
	grub> set root=(hd0,5) # el 1er disc dur és hd0, el 2on disc dur hd1, però la 1a partició és 1 ...
	```
	
	Carreguem el kernel:
	```
	grub> linux /boot/vmlinuz-4......  root=/dev/sda5
	```
	* Carreguem el disc ram inicial *initrd*:
	```
	grub> initrd /boot/initramfs-4....
	```
	* Finalment arrenquem:
	```
	grub> boot
	```
	
	* Alerta: encara que sembli redundant si ens oblidem *root=/dev/sda5*
	al carregar el kernel no es carregarà el sistema

#### PASSWORDS

En principi GRUB està configurat de manera que qualsevol usuari pot fer de tot amb grub: 
seleccionar qualsevol entrada del menú, editar-la o accedir a una shell del GRUB.

En certs casos (per exemple la creació d'un quiosc) ens pot convenir que l'accès a aquestes funcionalitats
sigui autenticat mitjançant passwords.

Abans de passar a la part pràctica respondrem a les següents preguntes ajudant-nos de l'enllaç
[GRUB2 Security](https://www.gnu.org/software/grub/manual/grub.html#Security)

4. Per poder tenir autenticació, hem d'activar una variable d'entorn que contindrà l'usuari administrador
( o els usuaris administradors), com es diu aquesta variable?
	
5. En el moment que activem la variable d'entorn anterior, els usuaris que no siguin administradors
poden editar les entrades del grub o accedir a la shell del grub?
	
6. I els no administradors poden escollir les diferents entrades del grub?

7. Quina o quines seran les paraules clau que faran que qualsevol pugui arrencar una certa entrada del menú
o que només la poguin escollir uns quants usuaris autenticats?
	
Fem ja la part pràctica. **IMPORTANT**: després d'editar el `grub.cfg.bkp` utilitzem `grub2-script-check`.


8. Suposem que la variable d'abans només conté un usuari de nom "root" i contrasenya "jupiter", 
tot i que l'ideal és escriure als scripts que es troben a */etc/grub.d/* no ho farem i escriurem directament a /boot/grub2/grub.cfg (*)
concretament a la secció */etc/grub.d/01_users* just després de l'etiqueta BEGIN posarem
	```
	set superusers="root"
	password root jupiter
	password hisx1 hisx1
	```
	Fes els canvis adients perquè tothom pugui accedir al Fedora de la tarda però només el superusuari i hisx1 puguin accedir al matí.

	Pot ser d'ajuda l'enllaç: [GRUB2/Password](https://help.ubuntu.com/community/Grub2/Passwords)

9. Dupliqueu l'entrada del matí. Ara voldrem que:
* El primer Fedora MATI només sigui accesible per root
* El segon Fedora MATI accesible per root i hisx1
* El Fedora TARDA per tothom

10. Fins ara hem treballat amb passwords no encriptats, per tant podrien ser fàcilment *crackejats*. 
Afortunadament GRUB2 ens proporciona encriptació mitjançant l'ordre ```grub2-mkpasswd-pbkdf2```
Fes el mateix d'abans, amb els mateixos passwords (*jupiter* i *hisx1* respectivament) 
però canviant el que sigui necessari per utilitzar aquests passwords encriptats.

#### Altres opcions

11. Com faries perquè arranquès l'entrada de FEDORA TARDA en 5 segons ?

12. Duplica una entrada de Fedora Mati i fes que arrenqui en mode monousuari.
